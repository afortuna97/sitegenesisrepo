'use strict';

/**
 * Controller that determines the page rendered when a customer accesses the site domain (www.mydomain.com).
 * The Start function that it exports points at the controller that renders the home page.
 * @module controllers/Default
 */

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

/**
 * This function is called when the site is turned offline (not live).
 */
function getView() {
    app.getView().render('testaccademy');
}

exports.GetView = guard.ensure(['get'], getView);
