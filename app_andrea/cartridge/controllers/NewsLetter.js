/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var ArrayList = require('dw/util/ArrayList');
var Mail = require('dw/net/Mail');
importPackage(dw.system);
importPackage(dw.object);

var logger = dw.system.Logger.getLogger('fafdsfsOTestLog', 'Balotelli');

function getView() {
	app.getView().render('newsletter/newsletter');
}

function deleted() {
	app.getView().render('newsletter/deleted');
}
//function test() { app.getView().render('error/siteoffline') }

function handleForm() {
	try {


		var email = request.httpParameterMap.email;

		var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry")
		var localServiceRegistry = LocalServiceRegistry.createService("newsletter", {
			createRequest: function (svc, params) {
				svc.addHeader('Content-Type', 'application/json');
				svc.addHeader('Access-Control-Request-Method', 'POST');
				return JSON.stringify(params);
			},
			parseResponse: function (svc, client) {
				return client.text;
			}
		});

		//Create request object
		var requestObject = {
			"properties": [
				{
					"property": "email",
					"value": email.toString()
				}
			]
		};

		//Send the request to OrderGroove

		var resp = localServiceRegistry.call(requestObject);
		var risposta = resp.msg;
		var statusCode = 0;
		statusCode = resp.getError();




		if (risposta.toString().trim() == "OK") {
			var vid = JSON.parse(resp.object).vid;
			sendmail(email, vid, "create");
		}



		if (risposta.toString().trim() != "OK" && risposta.toString().trim() != "Conflict") {
			Serverror(email);
		}
		//response.getWriter().println({statusCode:200,error:false,message:risposta});


		if (!!statusCode) {
			response.setStatus(statusCode);
		}
		else {
			response.getWriter().println(risposta);
		}


	} catch (error) {

		logger.info(error);
		response.setStatus(500);
		Serverror(email);
	}
}

function sendmail(Email, vid, action) {
	var email = new Mail();
	
	if (Array.isArray(Email)) {
		email.setTo(new ArrayList(Email));
	} else {
		email.addTo(Email);
	}
	email.setFrom("andrea@alpenite.it");


	if (action == "create") {
		email.setSubject("Registrazione NewsLetter");
		email.setContent("<div style='height:80%;width=80%;background-color:beige;'>Salve " + Email + ". Grazie per esserti iscritto alla newsletter! <br><a href='https://alpenite03-alliance-prtnr-eu05-dw.demandware.net/on/demandware.store/Sites-SiteGenesis-Site/en_US/NewsLetter-deletemail?vid=" + vid.toString() + "&email=" + Email.toString() + "'>Clicca qui</a> per togliere la registrazione</div>", 'text/html', 'UTF-8');
	}

	else if (action == "delete") {
		email.setSubject("Eliminazione NewsLetter");
		email.setContent("Email " + Email + " eliminata con successo", 'text/html', 'UTF-8');
		var t = "Email " + Email + " eliminata con successo";
		deleted();
	}
	else {
		alert("ERROR");
	}
	return email.send();
}


function deletemail(vid, email) {
	//https://api.hubapi.com/contacts/v1/contact/vid/${vid}?hapikey=7154acbe-d23a-49f0-ba84-8c014eec30a5
	var email = request.httpParameterMap.vid;
	var miaemail = request.httpParameterMap.email;
	var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry")
	var localServiceRegistry2 = LocalServiceRegistry.createService("newsletterdelete", {
		createRequest: function (svc) {
			svc.setURL("https://api.hubapi.com/contacts/v1/contact/vid/" + email + "?hapikey=7154acbe-d23a-49f0-ba84-8c014eec30a5");
			svc.setRequestMethod("DELETE");
		},
		parseResponse: function (svc, client) {
			return client.text;
		}
	});

	var resp = localServiceRegistry2.call();

	if (resp.toString().trim() == "[OK]") {
		sendmail(miaemail, 0, "delete");
	}

}

function Serverror(email) {
	try {
		Transaction.wrap(function () { var co = CustomObjectMgr.createCustomObject("newsletterSubscriber", email); });
	} catch (error) { }
}

//exports.Start = guard.ensure(['get'], test);
exports.getView = exports.getview = exports.GetView = guard.ensure(['get'], getView);
exports.HandleForm = guard.ensure(['post'], handleForm);
exports.deletemail = guard.ensure(['get'], deletemail);