/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var logger = dw.system.Logger.getLogger('testLog', 'Balotelli');

function getView()
 { 
    var prod=dw.catalog.ProductMgr.getProduct('008884304108');
    logger.info("testing");
    app.getView(
    {
        product:prod,
        test:'cds',
        array:['test','test2','test3']
    }
 ).render('testaccademy'); }

function test() { app.getView().render('error/siteoffline') }

exports.Start = guard.ensure(['get'], test);
exports.GetView = guard.ensure(['get'], getView);