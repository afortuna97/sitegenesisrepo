/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');
var ArrayList = require('dw/util/ArrayList');
var Mail = require('dw/net/Mail');
importPackage(dw.system);
importPackage(dw.object);

var logger = dw.system.Logger.getLogger('fafdsfsOTestLog', 'Balotelli');


exports.delete = function deleteObjects() {


}

function sendmail(Email, vid) {
    var email = new Mail();

    if (Array.isArray(Email)) {
        email.setTo(new ArrayList(Email));
    } else {
        email.addTo(Email);
    }
    email.setFrom("andrea@alpenite.it");

    email.setSubject("Registrazione NewsLetter");
    email.setContent("Salve " + Email + ". Grazie per esserti iscritto alla newsletter! <br><a href='https://alpenite03-alliance-prtnr-eu05-dw.demandware.net/on/demandware.store/Sites-SiteGenesis-Site/en_US/NewsLetter-deletemail?vid=" + vid.toString() + "&email=" + Email.toString() + "'>Clicca qui</a> per togliere la registrazione", 'text/html', 'UTF-8');

    return email.send();
}


exports.HandleForm = function handleForm() {
    Transaction.wrap(function () {
        try {

            var co = CustomObjectMgr.getAllCustomObjects("newsletterSubscriber");
            
            while (co.hasNext()) {
                var email = co.next()
                var email2 = email;
                logger.info(email);
                logger.info(email2);
                email = email.toString().replace("[CustomObject type=newsletterSubscriber,key=", "").replace("]", "");

                var LocalServiceRegistry = require("dw/svc/LocalServiceRegistry")
                var localServiceRegistry = LocalServiceRegistry.createService("newsletter", {
                    createRequest: function (svc, params) {
                        svc.addHeader('Content-Type', 'application/json');
                        svc.addHeader('Access-Control-Request-Method', 'POST');
                        return JSON.stringify(params);
                    },
                    parseResponse: function (svc, client) {
                        return client.text;
                    }
                });

                //Create request object
                var requestObject = {
                    "properties": [
                        {
                            "property": "email",
                            "value": email.toString()
                        }
                    ]
                };

                //Send the request to OrderGroove
                var resp = localServiceRegistry.call(requestObject);
                var statusCode = resp.getError();
                var risposta = resp.msg.toString();

                if (risposta.toString().trim() == "OK") {
                    var vid = JSON.parse(resp.object).vid;
                    sendmail(email, vid);
                }


                if (risposta.toString().trim() == "OK" || risposta.toString().trim() == "Conflict") {
                    CustomObjectMgr.remove(email2);
                }
            }
        } catch (error) {
            logger.error(error);
        };

    }
    );
};



